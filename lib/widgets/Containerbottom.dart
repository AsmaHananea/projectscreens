import 'package:flutter/material.dart';

class Bottom extends StatelessWidget {
  final Widget title ; 
  Bottom({
    required this.title}) ;


  @override
  Widget build(BuildContext context) {
    return Container(
                        height: MediaQuery.of(context).size.height*0.058,
                        width: MediaQuery.of(context).size.width*0.58,
                        decoration: BoxDecoration(
                          color : Color(0XFFff1e56) ,  
                          borderRadius: BorderRadius.circular(10) , 
                        ),
                        child: Center(
                          child:title , 
                          ),
                        
                    
                        );
  }
}