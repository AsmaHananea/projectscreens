import 'package:flutter/material.dart';
import 'package:homescreen/Screens/Connectingfile.dart';
import 'package:homescreen/Screens/FailedConnection.dart';
import 'package:homescreen/Screens/LoginWifi.dart';
import 'package:homescreen/Screens/HomePortrait.dart';
import 'package:homescreen/Screens/SeccefulConnection.dart';
import 'Screens/Connectwif.dart';

import 'package:device_preview/device_preview.dart';

void main() => runApp(
  DevicePreview(
   
    builder: (context) => MyApp(), // Wrap your app
  ),
);







class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false ,
      builder: DevicePreview.appBuilder, 

      routes: {
    
        HomePage1.id: (context) => const HomePage1(),
  
        HomePortrait.id: (context) =>  HomePortrait(),
        LoginWifi.id: (context) =>  LoginWifi(),
        ConnectingWifi.id: (context) =>  ConnectingWifi(),
         FailedConnection.id: (context) =>  FailedConnection(),
           SEccufulConnection.id: (context) =>  SEccufulConnection(),
  },
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage1(),
      
    );
  }
}
