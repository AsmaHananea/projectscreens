import 'package:flutter/material.dart';
import 'package:homescreen/Screens/Connectwif.dart';
import 'package:homescreen/widgets/Clipathes.dart';
import 'package:homescreen/widgets/Containerbottom.dart';

class HomePage1 extends StatefulWidget {
   static const id = 'homeportrait' ; 
  const HomePage1({ Key? key }) : super(key: key);

  @override
  _HomePage1State createState() => _HomePage1State();
}

class _HomePage1State extends State<HomePage1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFF323232),
      appBar : AppBar(
        centerTitle: true ,
        backgroundColor: Color(0XFFd8d8d8),
       title: Row(
           mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
         children: [
           Icon(Icons.all_inclusive , color: Colors.black),
           SizedBox(width: 4,) ,
           Text('SMPL.TV' , style: TextStyle(color: Colors.black , 
           fontSize: 15 , 
           fontWeight: FontWeight.w900 , 
           ),),
       ],),
      ) , 
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Center(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 160),
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, HomePortrait.id) ; 
                      },
                      child: Bottom(title: Text(
                            'Connect To My Home ' , 
                            style : TextStyle(
                              color: Colors.white , 
                              fontWeight: FontWeight.w700 , 
                              fontSize: 15 , 
                    
                    
                            ),  ) , 
                      ) , 
                    ),

                    ),
                    SizedBox(height: 13,), 
                  Text('Choose this if you already have home Wifi' ,
                  style : TextStyle(
                              color: Colors.grey , 
                              fontWeight: FontWeight.w400 , 
                              fontSize: 16 , ), 
                  ) ,
                    SizedBox(height: 23,), 
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 120),
                          child: Divider(
                            // height: 35,
                            color: Colors.grey,
                           
                            ),
                        ), 
                       Center(
                         child: CircleAvatar(
                           radius: 8,
                           backgroundColor:Color(0XFF323232) ,
                           child: Text('or' , 
                            style : TextStyle(
                              color: Colors.grey , 
                              fontWeight: FontWeight.w500 , 
                              fontSize: 14 , ), 
                           ),
                           
                         ),
                       )
                            
                          
                        
                      ],

                    ), 
                     SizedBox(height: 23,), 
                     GestureDetector(
                      onTap: (){},
                      child: Bottom(title: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                                'USE ' , 
                                style : TextStyle(
                                  color: Colors.white , 
                                  fontWeight: FontWeight.w700 , 
                                  fontSize: 15 , 
                    
                    
                                ),  ),
                                Icon(Icons.all_inclusive , color: Colors.white,), 
                                 Text(
                                'SMPL.TV Wifi' , 
                                style : TextStyle(
                                  color: Colors.white , 
                                  fontWeight: FontWeight.w700 , 
                                  fontSize: 15 , 
                    
                    
                                ),  ),

                        ],
                      ) , 
                    ),),

                    
                    SizedBox(height: 13,), 
                  Text('Choose this if you do not have home Wifi' ,
                  style : TextStyle(
                              color: Colors.grey , 
                              fontWeight: FontWeight.w400 , 
                              fontSize: 16 , ), 
                  ) ,
                    SizedBox(height: 20,), 
                ],
              ),
            ) , 
          ),
          Positioned(
               top : 0 , 
               child :  Stack(
                 children: [
                   Container(
                     color : Color(0XFF323232) ,  
                     height: 120,
                     width: MediaQuery.of(context).size.width,
                     child: ClipPath(
                                      child: Container(
                                          width:  MediaQuery.of(context).size.height*0.02,
                                            
                                            height: MediaQuery.of(context).size.height*0.01,
                                            color: Color(0XFF2c2a2a),
                                      ),
                                      clipper: CustomClipPathe1(),
                                    ),
                   ),
                   
                    
                        
                    Container(
                      height: 70,
                     width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Text('SET UP YOUR DEVICE' , 
                         style: TextStyle(
                           color: Color(0XFFffac41) ,
                           fontSize: 20 , 
                           fontWeight: FontWeight.w500 , 
                         
                          ),
                   ),
                      ),
                    ) ,
                 ],
               ),

             ) , 
        ],
      ),
    );
  }
}

  