import 'package:flutter/material.dart';
import 'package:homescreen/Screens/Connectwif.dart';
import 'package:homescreen/Screens/FailedConnection.dart';
import 'package:homescreen/widgets/Clipathes.dart';
import 'package:homescreen/widgets/Containerbottom.dart';

class ConnectingWifi extends StatefulWidget {
   static const id = 'connection' ; 
  const ConnectingWifi({ Key? key }) : super(key: key);

  @override
  _HomePage1State createState() => _HomePage1State();
}

class _HomePage1State extends State<ConnectingWifi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFF323232),
      appBar : AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true ,
        backgroundColor: Color(0XFFd8d8d8),
       title: Row(
           mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
         children: [
           Icon(Icons.all_inclusive , color: Colors.black),
           SizedBox(width: 4,) ,
           Text('SMPL.TV' , style: TextStyle(color: Colors.black , 
           fontSize: 15 , 
           fontWeight: FontWeight.w900 , 
           ),),
       ],),
      ) , 
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 80 , vertical: 160),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                
                children: [
                   Text('Name ' , style: TextStyle(
                            color: Colors.grey ,
                             fontWeight: FontWeight.w600),) ,
                             SizedBox(height: 10,) , 
                   Container(
                     height: 40,
                     width: 170,
                     decoration: BoxDecoration(
                       color:  Color(0XFF2c2a2a),
                       borderRadius: BorderRadius.circular(10)
                     ),
                     child: Padding(
                       padding: const EdgeInsets.all(10.0),
                       child: Text('NottaFinger !', 
                       style: TextStyle(color: Colors.white , 
           fontSize: 15 , 
           fontWeight: FontWeight.w700 , 
           ),
                       ),
                     ),
                   ),
                   SizedBox(height: 19,), 

                    Center(
                      child: Text('Pleas Wait...' ,
                        style: TextStyle(color: Color(0XFFffac41) ,
           fontSize: 15 , 
           fontWeight: FontWeight.w300 , 
           ),
                      ),
                    ) ,    
                       SizedBox(height: 19,),      
                GestureDetector(
                  onTap: (){
                    Navigator.pushNamed(context, FailedConnection.id); 

                  },
                  child: Container(
                    child: Icon(Icons.all_inclusive,color:Colors.white ,
                    size: 40, )
                
                  ),
                ),




                  
                ],
              ),
            ) , 
          ),
          Positioned(
               top : 0 , 
               child :  Stack(
                 children: [
                   Container(
                     color : Color(0XFF323232) ,  
                     height: 120,
                     width: MediaQuery.of(context).size.width,
                     child: ClipPath(
                                      child: Container(
                                          width:  MediaQuery.of(context).size.height*0.02,
                                            
                                            height: MediaQuery.of(context).size.height*0.01,
                                            color: Color(0XFF2c2a2a),
                                      ),
                                      clipper: CustomClipPathe1(),
                                    ),
                   ),
                   Container(
                    height: 75,
                   width: MediaQuery.of(context).size.width,
                     child: Center(
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Text('CONNECTING' , 
                           style: TextStyle(
                             color: Color(0XFFffac41) ,
                             fontSize: 21 , 
                             fontWeight: FontWeight.w500 , 
                           ),
                            ),
                            Text('WIFI' , 
                           style: TextStyle(
                             color: Color(0XFFffac41) ,
                             fontSize: 21 , 
                             fontWeight: FontWeight.w500 , 
                           ),
                            ),
                         ],
                       ),
                     ),
                   ) ,
                 ],
               ),

             ) , 
        ],
      ),
    );
  }
}