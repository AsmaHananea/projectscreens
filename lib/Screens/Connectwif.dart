import 'package:flutter/material.dart';
import 'package:homescreen/Screens/LoginWifi.dart';
import 'package:homescreen/widgets/Clipathes.dart';
import 'package:homescreen/widgets/Wifi.dart';

class HomePortrait extends StatefulWidget {
   static const id= 'Secondhome portrait' ; 
   bool enabled = false ; 


  

  @override
  _HomePortraitState createState() => _HomePortraitState();
}

class _HomePortraitState extends State<HomePortrait> {
 
 bool enabled = false ; 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       bottomNavigationBar: BottomAppBar(color: Color(0XFFd8d8d8),
       child: Container(
                   decoration: BoxDecoration(
                     color : Color(0XFF323232) ,
                   border: Border.all(
                     color : Color(0XFF323232) ,
                   )
                 ),
                 
                 height: MediaQuery.of(context).size.height*0.089,
                 width: MediaQuery.of(context).size.width,
                 child: ClipPath(
                                  child: Container(
                                      width:  MediaQuery.of(context).size.height*0.02,
                                        
                                        height: MediaQuery.of(context).size.height*0.01,
                                        color: Color(0XFF2c2a2a),
                                  ),
                                  clipper: CustomClipPath(),
                                ),
               ),
       ),

      backgroundColor: Color(0XFF323232),
      appBar : AppBar(
        automaticallyImplyLeading: false,
       
        centerTitle: true ,
        backgroundColor: Color(0XFFd8d8d8),
       title: Row(
           mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
         children: [
           Icon(Icons.all_inclusive , color: Colors.black),
           SizedBox(width: 4,) ,
           Text('SMPL.TV' , style: TextStyle(color: Colors.black , 
           fontSize: 15 , 
           fontWeight: FontWeight.w900 , 
           ),),
       ],),
      ) , 
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment : CrossAxisAlignment.start , 
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20 , left: 10),
              child: GestureDetector(
                onTap: (){
                  Navigator.pop(context) ; 
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0XFFffac41) , 
                    borderRadius: BorderRadius.circular(10)
                  ),
                  height: 35,
                  width: 100,
                   child: Center(
                     child: Row(
                       
                       children: [
                        Icon(Icons.arrow_left , size :37) , 
                     
                        Text('BACK' , 
                        style: TextStyle(
                         
                          fontWeight: FontWeight.bold , 
                          fontSize: 14
                        ),
                        ) ,
                
                      ],),
                   ),
                    
                ),
              ),
            ),
           
            SizedBox(
              height : MediaQuery.of(context).size.height*0.015 , 
          
          
            )  ,
            Center(
              child: Text('AVAILABALE WIFI NETWORKS' , 
              style : TextStyle(
                color : Color(0XFFffac41),
                fontSize: MediaQuery.of(context).size.height*0.021, 
                fontWeight: FontWeight.w600  ,
                
              ),
              ),
            ) , 
            Stack(
              children: [
                
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 40, 
                    vertical: 14
                  ),
                  child: Center(
                    child: Container(
                      height:MediaQuery.of(context).size.height*0.58,
                      
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Color(0XFF8e8e8e) , 
                            
                            ) , 
                            borderRadius: BorderRadius.circular(18) , 
                      
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20 , right: 20 , top: 20) , 
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: MediaQuery.of(context).size.height*0.2,
                              color: Color(0XFF2c2a2a),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 5 , right: 5),
                                child: Column(
                                  children: [
                                    Wifi(
                                      title: 'User Home Wifi 5G', 
                                      image: Image(image: AssetImage('images/wifi1.png',),) , ),
                                        Wifi(
                                      title: 'User Home Wifi ', 
                                      image: Image(image: AssetImage('images/wifi1.png',),) , ),
                                        Wifi(
                                      title: 'User Guest Wifi ', 
                                      image: Image(image: AssetImage('images/wifi2.png',),) , ),
                                        Wifi(
                                      title: 'Xfinity', 
                                      image: Image(image: AssetImage('images/wifi2.png',),) , ),
                                        Wifi(
                                      title: 'Weiried Nighboor Open ...', 
                                      image: Image(image: AssetImage('images/wifi3.png',),) , ), 
                                      Wifi(
                                      title: 'In-Laws Wifi', 
                                      image: Image(image: AssetImage('images/wifi3.png',),) , ), 
                                      Wifi(
                                      title: 'Nottafinger', 
                                      image: Image(image: AssetImage('images/wifi4.png',),) , ), 
                                      Wifi(
                                      title: 'FGCBNG324getit', 
                                      image: Image(image: AssetImage('images/wifi3.png',),) , ), 
                                    
                                   
          
                                  ],
                                ),
                              ) , 
                            
                           
      
                            ) , 
                            SizedBox(height: 16 ,) ,
                            Text('Name ' , style: TextStyle(
                              color: Colors.grey ,
                               fontWeight: FontWeight.w600),) ,
                            SizedBox(height: MediaQuery.of(context).size.height*0.014,) , 
                            Container(
                              height: 35,
                                color: Color(0XFF2c2a2a),
      
                    
                    child: TextField(
                      
                      cursorColor: Colors.grey ,
                       cursorHeight :23 , 
                      
                      
                      style: TextStyle(color: Colors.grey , fontWeight: FontWeight.w700),
                     decoration: new InputDecoration(
                       contentPadding: EdgeInsets.only(  bottom: 35 / 2,),
                     border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                     enabledBorder: InputBorder.none,
                     errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      
                      
                      ),
                      

                      onChanged: (text) {
                        
                      },
                    ) , ),
                            SizedBox(height: MediaQuery.of(context).size.height*0.026,) , 
                            Text('Password ' , style: TextStyle(
                              color: Colors.grey ,
                               fontWeight: FontWeight.w600),) ,
                          SizedBox(height: MediaQuery.of(context).size.height*0.014,) , 
                            Container(
                              height: 35,
                                color: Color(0XFF2c2a2a),
      
                    margin: EdgeInsets.all(2),
                    child: TextField(
                      obscureText: true,
                      
                     cursorColor: Colors.grey ,
                       cursorHeight :23 , 
                      
                      
                      style: TextStyle(color: Colors.grey , fontWeight: FontWeight.w700),
                     decoration: new InputDecoration(
                       contentPadding: EdgeInsets.only(  bottom: 35 / 2,),
                     border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                     enabledBorder: InputBorder.none,
                     errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      
                      
                      ),
                      onChanged: (text) {
                        
                      },
                    ) , 
                    ),
                          SizedBox(height: MediaQuery.of(context).size.height*0.03,) ,  
                             Center(
                      child: GestureDetector(
                        onTap: (){
                          Navigator.pushNamed(context, LoginWifi.id) ; 
                       
                        },
                        child: Container(
                          height :  MediaQuery.of(context).size.height*0.04  , 
                          width : 140 , 
                          decoration: BoxDecoration(
                            color: Color(0XFFff1e56) , 
                            borderRadius: BorderRadius.circular(8) , 
                            
                      
                          ),
                          child: Center(child: Text('Connect' , 
                          style: TextStyle(
                            color: Colors.white , 
                            fontWeight: FontWeight.w500 , 
                            fontSize: 15 , 
                          ),
                          )),
                        ),
                      ),
                    )
                            
                          ],
                        ),
                      ),
          
                    ),
                  ),
                ),
                Positioned(
                  top: 6,
                  left: MediaQuery.of(context).size.width*0.5 -60,
                  
                  
                  
      
                  child: Container(
                  color: Color(0XFF323232),
                  height : 20 , 
                  width: 140,
                  child: Text('Select your Network' , 
                  style: TextStyle(
                    color: Colors.grey , 
                    fontSize : 15 , 
      
                  ),
                
                  ),
                   
      
                )) ,
                
              ],
            ) , 
          
          ],
        ),
      ),
    );
  }
}
