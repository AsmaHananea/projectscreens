import 'package:flutter/material.dart';

class SEccufulConnection extends StatefulWidget {
  static const id ='Secceful';
  const SEccufulConnection({ Key? key }) : super(key: key);

  @override
  _SEccufulConnectionState createState() => _SEccufulConnectionState();
}

class _SEccufulConnectionState extends State<SEccufulConnection> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: Color(0XFF323232),
      appBar : AppBar(
        automaticallyImplyLeading : false , 
        centerTitle: true ,
        backgroundColor: Color(0XFFd8d8d8),
       title: Row(
           mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
         children: [
           Icon(Icons.all_inclusive , color: Colors.black),
           SizedBox(width: 4,) ,
           Text('SMPL.TV' , style: TextStyle(color: Colors.black , 
           fontSize: 15 , 
           fontWeight: FontWeight.w900 , 
           ),),
       ],),
      ) , 
      body: SingleChildScrollView(
        child: Column(
        
          
          
          children: [
            Container(
              width : MediaQuery.of(context).size.width , 
              height: MediaQuery.of(context).size.height*0.08 , 
              child :  Padding(
              padding: const EdgeInsets.only(left: 10 , top: 16 , right: 230 ),
              child: GestureDetector(
                onTap: (){
                  Navigator.pop(context) ; 
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0XFFffac41) , 
                    borderRadius: BorderRadius.circular(10)
                  ),
                  
                   child: Center(
                     child: Row(
                       
                       children: [
                        Icon(Icons.arrow_left , size :37) , 
                     
                        Text('BACK' , 
                        style: TextStyle(
                         
                          fontWeight: FontWeight.bold , 
                          fontSize: 14
                        ),
                        ) ,
                
                      ],),
                   ),
                    
                ),
              ),
            ),
            ), 
              
              SizedBox(height: 30) , 
              Container(
                     height: 40,
                     width: 200,
                     decoration: BoxDecoration(
                       color:  Color(0XFF2c2a2a),
                       borderRadius: BorderRadius.circular(10)
                     ),
                     child: Padding(
                       padding: const EdgeInsets.all(10.0),
                       child: Text('User Home WIFI 5G ', 
                       style: TextStyle(color: Colors.white , 
           fontSize: 15 , 
           fontWeight: FontWeight.w700 , 
           ),
                       ),
                     ),
                   ),
                   SizedBox(height: 19,), 

                    Center(
                      child: Text('Connection Successful' ,
                        style: TextStyle(color: Color(0XFFffac41) ,
           fontSize: 15 , 
           fontWeight: FontWeight.w300 , 
           ),
                      ),
                    ) ,    
                       SizedBox(height: 19,),      
                GestureDetector(
                  onTap: (){
                   

                  },
                  child: Container(
                    child: Icon(Icons.all_inclusive,color:Colors.white ,
                    size: 40, )
                
                  ),
                ),
              

          ],
        ),
      )
    );
  }
}