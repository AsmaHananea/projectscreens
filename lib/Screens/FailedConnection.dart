import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:homescreen/Screens/SeccefulConnection.dart';

class FailedConnection extends StatelessWidget {
  static const id ='Failed'; 
  const FailedConnection({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(

      backgroundColor: Color(0XFF323232),
      appBar : AppBar(
        automaticallyImplyLeading : false , 
        centerTitle: true ,
        backgroundColor: Color(0XFFd8d8d8),
       title: Row(
           mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
         children: [
           Icon(Icons.all_inclusive , color: Colors.black),
           SizedBox(width: 4,) ,
           Text('SMPL.TV' , style: TextStyle(color: Colors.black , 
           fontSize: 15 , 
           fontWeight: FontWeight.w900 , 
           ),),
       ],),
      ) , 
      body: SingleChildScrollView(
        child: Column(
        
          
          
          children: [
            Container(
              width : MediaQuery.of(context).size.width, 
              height: MediaQuery.of(context).size.height*0.078 , 
              child :  Padding(
              padding: const EdgeInsets.only(left: 10 , top: 16 , right: 220 ),
              child: GestureDetector(
                onTap: (){
                  Navigator.pop(context) ; 
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0XFFffac41) , 
                    borderRadius: BorderRadius.circular(10)
                  ),
                  
                   child: Center(
                     child: Row(
                       
                       children: [
                        Icon(Icons.arrow_left , size :37) , 
                     
                        Text('BACK' , 
                        style: TextStyle(
                         
                          fontWeight: FontWeight.bold , 
                          fontSize: 14
                        ),
                        ) ,
                
                      ],),
                   ),
                    
                ),
              ),
            ),
            ), 
              
              SizedBox(height: 30) , 
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0),
                child: Column(
                  children: [
                    Text('FAILED ' , 
                    style : TextStyle(
                      color: Color(0XFFff1e56) ,
                      fontSize: 21 , 
                      fontWeight: FontWeight.w400 , 
      
                    ),
                    
                    ),
                     Text('CONNECTION ' , 
                    style : TextStyle(
                      color: Color(0XFFff1e56) ,
                      fontSize: 21 , 
                      fontWeight: FontWeight.w400 , 
      
                    ),
                    
                    ),
                  ],
                ),
              ) , 
              SizedBox(height: 35,) , 
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35),
                child: GestureDetector(
                  onTap : (){
                    Navigator.pushNamed(context, SEccufulConnection.id) ; 

                  }, 
                  child: Container(
                        height:MediaQuery.of(context).size.height*0.1,

                        
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0XFF8e8e8e) , 
                              
                              ) , 
                              borderRadius: BorderRadius.circular(18) , 
                        
                        ),
                        child: Center(
                          child: Text('ERROR : COULD NOT CONNECT' , 
                          style: TextStyle(
                            color: Color(0XFFffac41) , 
                            fontWeight: FontWeight.w300 , 
                            fontSize: 16
                          ),
                          ),
                        ),
                
                        ),
                ),
              ), 
              

          ],
        ),
      )
    );
  }
}